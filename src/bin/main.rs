extern crate zip;
extern crate xml;
extern crate csv;
extern crate regex;
use crate::zip::read::{ZipArchive};
use crate::zip::result::{ZipResult};
use std::fs::File;
use xml::reader::{EventReader, XmlEvent};
use std::collections::{HashMap};
use std::string::String;
use std::env;
use std::io::Write;
use std::io::BufRead;
use regex::Regex;

fn main()  {
    let m : (HashMap<String, Vec<String>>, HashMap<String, Ects>) = (HashMap::new(), HashMap::new());
    let args : Vec<String> = env::args().collect();
    let all_effects = read_effects(args[1].as_str()).unwrap();
    let eff : ZipResult<(HashMap<String, Vec<String>>, HashMap<String, Ects>)> = args.iter().skip(2).try_fold(m, |(mut acc, mut ee), i| {
	let (mm, ects) = read_content(&i)?;
	acc.extend(mm);
	ee.extend(ects);
	Ok((acc, ee))
    });
    let (effects, ects) = match eff {
        Ok(s) => s,
        Err(e) => panic!("Problem opening file {:?}" , e)
    };
    println!("Przedmioty: {:?} {:?}", effects, ects);

    let (matrix, modules) = effects_to_matrix(&effects, &all_effects);
    match write_effects_matrix(File::create("output.csv").unwrap(), &matrix, &modules, &all_effects) {
	Ok(()) => (),
	Err(e) => panic!("Problem writing matrix {}", e)
    }

    match save_ects_table("ects.csv", &ects) {
	Ok(()) => (),
	Err(e) => panic!("Problem writing ects matrix {}", e)
    }
}

fn save_ects_table(path: &str, ects: &HashMap<String, Ects>) -> csv::Result<()> {
    let file = File::create(path)?;
    let mut writer = csv::Writer::from_writer(file);
    writer.write_record(vec!["Przedmiot", "ECTS", "Kontaktowe", "Praktyczne"])?;
    ects.iter().try_for_each(|(module, ects)| writer.write_record(vec![module, &ects.all.to_string(), &ects.contact.to_string(), &ects.practical.to_string()]))
}

fn get_ects_sum(str: &str) -> Option<u8> {
    let re = Regex::new(r"co odpowiada( przynajmniej)? (\d+) (pkt\.|punktom) ECTS").unwrap();
    re.captures_iter(str).next().map(|x| {x[2].parse().ok()}).flatten()
}

fn read_content(path: &str) -> ZipResult<(HashMap<String, Vec<String>>, HashMap<String, Ects>)> {
    let all_ects_marker = vec!["Liczba godzin pracy studenta związanych z osiągnięciem efektów uczenia się".to_owned(), "Liczba godzin pracy studenta związanych z osiągnięciem efektów kształcenia".to_owned()];
    let practical_ects_marker = vec!["praktycznym".to_owned()];
    let contact_ects_marker = vec!["nauczycieli".to_owned(), "akademickich".to_owned()];
    let mut all_effects : HashMap<String, Vec<String>> = HashMap::new();
    let mut effects : Option<(String, Vec<String>)> = Option::None;
    let file = File::open(path)?;
    let mut zip = ZipArchive::new(file)?;
    let contents = zip.by_name("content.xml")?;
    let parser = EventReader::new(contents);
    let mut in_table: u32 = 0;
    let mut name_cell: bool = false;
    let mut in_effects: bool = false;
    let mut in_footnote: bool = false;
    let mut eff_contents : Vec<String> = Vec::new();
    let mut ects = EctsFullExtractor{
	all_extractor: EctsSingleExtractor::create(all_ects_marker),
	practical_extractor: EctsSingleExtractor::create(practical_ects_marker),
	contact_ectractor: EctsSingleExtractor::create(contact_ects_marker),
	ects: HashMap::new()
    };
    let mut context = Context{table_column: 0};
    
    for e in parser {
        match e {
            Err(e) => println!("parse error {:?}", e),
            Ok(XmlEvent::StartElement{name, ..}) if name.local_name == "table" => in_table = in_table+1,
            Ok(XmlEvent::EndElement{name, ..}) if name.local_name == "table" => {
                in_table = in_table-1;
                if in_effects && in_table == 0 { in_effects = false; }
            },
	    Ok(XmlEvent::StartElement{name, ..}) if name.local_name == "annotation" => in_footnote=true,
	    Ok(XmlEvent::EndElement{name, ..}) if name.local_name == "annotation" => in_footnote=true,
	    Ok(XmlEvent::StartElement{name, ..}) if name.local_name == "note" => in_footnote=true,
	    Ok(XmlEvent::EndElement{name, ..}) if name.local_name == "note" => in_footnote=false,
            Ok(XmlEvent::StartElement{name, ..}) if name.local_name == "table-row" => context.table_column=0,
            Ok(XmlEvent::StartElement{name, ..}) if name.local_name == "table-cell" => context.table_column += 1,
	    Ok(XmlEvent::EndElement{name, ..}) if name.local_name == "table-row" && in_effects =>  {
		let ch = eff_contents.join("");
		if ch != "" {
                    match effects {
			Some((_, ref mut ef)) => {
                            let v: Vec<&str> = ch.split(',').flat_map(|x| x.split(' ')).collect();
                            for s in v {
				if s.trim() != "()" {
				    ef.push(s.trim().to_string());
				}
                            }
			},
			None => ()
                    }
		    eff_contents = Vec::new();
		}
                ()    
            },
            Ok(XmlEvent::Characters(ch)) if (ch == "OPIS EFEKTÓW UCZENIA SIĘ" || ch == "OPIS EFEKTÓW KSZTAŁCENIA") && in_table > 0  => in_effects=true,
            Ok(XmlEvent::Characters(ch)) if name_cell && context.table_column == 2 => {
                name_cell=false;
                match effects.replace((ch.clone(), Vec::new())) {
                    Some((c, e)) => {ects.end_module(&c); all_effects.insert(c, e);},
                    None => ()
                        
                };
		ects.new_module(&ch);
            }
            Ok(XmlEvent::Characters(ch)) if ch.contains("Nazwa przedmiotu") && (ch.contains("polskim") || !ch.contains("angielskim")) => name_cell=true,
            Ok(XmlEvent::Characters(ch)) if in_effects && context.table_column == 4 && !ch.contains("Odniesienie") && !in_footnote => eff_contents.push(ch.clone()),
            Ok(ev) => {
		ects.process_event(&ev, &context)
	    }
        }
    }

    match effects.take() {
        Some((c, e)) => {ects.end_module(&c);all_effects.insert(c, e);},
        None => ()            
    }
    
    return Ok((all_effects, ects.ects));
}

fn read_effects(path: &str) -> std::io::Result<Vec<String>> {
    let file = File::open(path)?;
    std::io::BufReader::new(file).lines().collect()
}

fn effects_to_matrix<'a, 'b>(effects: &'a HashMap<String, Vec<String>>, all_effects: &'b [String]) -> (Vec<Vec<i32>>, Vec<&'a String>) {
    let mut effect_matrix = vec![vec![0;all_effects.len()];effects.len()];

    let mut eff_vec: Vec<&'a String> = vec![];
    let eff_idx : HashMap<& String, usize> = all_effects.iter().enumerate().map(|(a,b)| (b,a)).collect();
    
    let bump_effect_coverage =|cov_vec: &mut Vec<i32>, effect: & String|  {
	match eff_idx.get(effect) {
	    Some(i) => cov_vec[*i]+=1,
	    None => println!("No effect for code {}", effect)
	}
    };
    
    effects.iter().enumerate().for_each(|(idx, (module, eff))| {
	eff_vec.push(module);
	let mut low_coverage = vec![0;all_effects.len()];
	let mut reg_coverage = vec![0;all_effects.len()];
	let mut full_coverage = vec![0;all_effects.len()];

	eff.iter().for_each(|eff| {
	    if eff.ends_with('-') {
		bump_effect_coverage(&mut low_coverage, &String::from(eff.strip_suffix("-").unwrap()));
	    } else if eff.ends_with('+') {
		bump_effect_coverage(&mut full_coverage, &String::from(eff.strip_suffix("+").unwrap()));
	    } else {
		bump_effect_coverage(&mut reg_coverage, eff);
	    }
	});

	effect_matrix[idx] = low_coverage.iter().zip(reg_coverage.iter().zip(full_coverage.iter()))
	    .map(|(low, (reg, full))|
		      if full > &0 || reg >= &3 {
			  3
		      } else if reg > &0 || low >= &3 {
			  2
		      } else if low > &0 {
			  1
		      } else {
			  0
		      }
	).collect();
    });
					

    (effect_matrix, eff_vec)
}

fn repeat_str(c: char, n: i32) -> String {
    let mut s = String::default();
    let mut i = 0;
    while n > i {
	s.push(c);
	i+=1;
    }
    s
}

fn write_effects_matrix<W: Write>(
    output: W, matrix: &Vec<Vec<i32>>, mod_labels: &Vec<&String>, eff_labels: &Vec<String>
) -> csv::Result<()> {
    let mut writer = csv::Writer::from_writer(output);
    let mut row1 : Vec<String> = Vec::default();
    row1.push("".to_owned());
    mod_labels.iter().for_each(|x| row1.push(x.to_string()));
    row1.push("Suma".to_owned());
    writer.write_record(row1)?;
    eff_labels.iter().enumerate()
	.try_for_each(|(eidx, eff)| {
	    let mut row : Vec<String> = Vec::default();
	    row.push(String::from(eff));
	    let mut sum = 0;
	    (0..mod_labels.len()).for_each(|x| {row.push(repeat_str('X', matrix[x][eidx])); sum+=matrix[x][eidx]});
	    row.push(sum.to_string());
	    writer.write_record(row)
	})
}

struct Context {
    table_column: u8
}

#[derive(Debug)]
struct Ects {
    all: u8,
    practical: u8,
    contact: u8
}

struct EctsSingleExtractor {
    descriptions: Vec<String>,
    collected_values: Option<String>,
    collected_ects: Option<u8>
}

impl EctsSingleExtractor {
    pub fn process_event(&mut self,  ev: &XmlEvent, context: &Context) {
	match self.collected_values {
	    Some(ref mut str) => match ev {
		XmlEvent::Characters(ch) if context.table_column == 2 => str.push_str(ch),
		XmlEvent::EndElement{name, ..} if name.local_name == "table-cell" && context.table_column == 2 => {
		    self.collected_ects = get_ects_sum(str);
		    self.collected_values=None
		},
		_ => ()
	    },
	    None => match ev {
		XmlEvent::Characters(ch) if self.descriptions.iter().any(|s| ch.contains(s)) => self.collected_values = Some(String::new()),
		_ => ()
	    }
	}
    }

    pub fn new_module(&mut self,  name: &str) {
	assert_eq!(self.collected_values, None);
	self.collected_ects = None
    }

    pub fn create(descriptions: Vec<String>) -> EctsSingleExtractor {
	EctsSingleExtractor{
	    collected_ects: None,
	    collected_values: None,
	    descriptions
	}
    }
}

struct EctsFullExtractor {
    all_extractor: EctsSingleExtractor,
    practical_extractor: EctsSingleExtractor,
    contact_ectractor: EctsSingleExtractor,
    ects: HashMap<String, Ects>
}

impl EctsFullExtractor {
    pub fn process_event(&mut self,  ev: &XmlEvent, context: &Context) {
	self.all_extractor.process_event(ev, context);
	self.practical_extractor.process_event(ev, context);
	self.contact_ectractor.process_event(ev, context);
    }

    pub fn new_module(&mut self, name: &str) {
	self.all_extractor.new_module(name);
	self.practical_extractor.new_module(name);
	self.contact_ectractor.new_module(name);
    }

    pub fn end_module(&mut self, name: &str) {
	self.ects.insert(name.to_string(), Ects{
	    all: self.all_extractor.collected_ects.unwrap_or_else(|| {println!("No all ects found for {}", name); 0}),
	    practical: self.practical_extractor.collected_ects.unwrap_or_else(|| {println!("No practical ects found for {}", name); 0}),
	    contact: self.contact_ectractor.collected_ects.unwrap_or_else(|| {println!("No contact ects found for {}", name); 0}),
	});
    }
}
